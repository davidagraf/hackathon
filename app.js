'use strict';

var express = require('express'),
    app = express(),
    path = require('path');

// configure express
app.configure(function() {
  app.use(express.compress()); // gzip / deflate
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(express.static(path.join(__dirname, '.tmp')));
  app.use(express.favicon());
  app.use(express.logger('dev'));
  // http://expressjs.com/api.html#bodyParser
  app.use(express.json()); // defines bodyParser without...
  app.use(express.urlencoded()); // ...file upload, more secure!
  app.use(express.cookieParser());
  app.use(express.session({ secret: 'securedsession' }));
});

app.get('/hello', function(req, res){
  res.send('Hello World');
});

app.get('/data', function(req, res){
  res.send(
    [
      {
        name: 'simon',
        amount: 10000
      },
      {
        name: 'andres',
        amount: 324234
      },
      {
        name: 'david',
        amount: 1
      },
      {
        name: 'adem',
        amount: -1
      }
    ]
  );
});

var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});
