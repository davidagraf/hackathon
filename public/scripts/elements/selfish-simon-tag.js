Polymer('selfish-simon-tag', {
  ready: function() {
      
    var ajax = this.$.ajax;

    // Respond to events it fires.
    ajax.addEventListener('polymer-response', function(e) {
      console.log(this.response);
    });
    ajax.go(); // Call its API methods.
      
    this.salutations = [
      { what: 'Hello', who: 'World'},
      { what: 'Goodbye', who: 'DOM APIs'},
      { what: 'Hello', who: 'Declarative'},
      { what: 'Goodbye', who: 'Imperative'}
    ];

    this.test = "this is a test";

    this.alternates = [ 'Hello', 'Hola', 'Howdy'];
    this.current = 0;
  },     
  updateModel: function() {
    this.current = (this.current + 1) % this.alternates.length;
    this.salutations[0].what = this.alternates[this.current];
  }
});
