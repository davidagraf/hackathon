'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();

gulp.task('scripts', function () {
    return gulp.src(['public/scripts/**/*.js','app.js'])
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe($.size());
});
 
gulp.task('serve', function () {
  $.nodemon({ script: 'app.js'});
});
